package zahlensortierer;

import java.util.*;


/**
 * @author Saarujan Sathiskumar
 *
 */
public class Zahlensortierer {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("1. Zahl: ");
		int zahl1 = myScanner.nextInt();
		System.out.print("2. Zahl: ");
		int zahl2 = myScanner.nextInt();
		System.out.print("3. Zahl: ");
		int zahl3 = myScanner.nextInt();
		
		int x = zahl1;
		if(x < zahl2) {		
			x = zahl2;
			}
		if(x < zahl3) {		
			x = zahl3;
			}
		
		int y = zahl1;
		if(y > zahl2) {		
			y = zahl2;
			}
		if(y > zahl3) {		
			y = zahl3;
			}
		System.out.println("Die groesste Zahl ist :" + x);
		System.out.println("Die kleinste Zahl ist :" + y);			
		myScanner.close();
	}
}